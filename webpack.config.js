module.exports = {
  entry: './simple-image-editor.jsx',
  output: {
    path: __dirname,
    filename: './dist/bundle.js',
    sourceMapFilename: './dist/simple-image-editor.map',
    library: 'simple-image-editor',
    libraryTarget: 'umd',
    devtoolModuleFilenameTemplate: '../[resource-path]',
  },
  externals: {
    'react': 'react',
    'react-dom': 'react-dom',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        enforce: 'pre',
        exclude: /node_modules/,
        loader: "eslint-loader",
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      }
    ]
  }
};
