import React from 'react';
import PropTypes from 'prop-types';
import Draggable from 'react-draggable';

const HANDLE_SIZE = 10;

class Handle extends React.Component {
  handleDrag = (event, ui) => {
    this.props.onMove(ui.position);
  };

  handleStop = (event, ui) => {
    this.props.onMove(ui.position);
  };

  render() {
    const style = {
      width: HANDLE_SIZE,
      height: HANDLE_SIZE,
      backgroundColor: this.props.color,
    };
    const h = <div className="handle" style={style} />;

    return (
      <Draggable
        {...this.prps}
        axis="x"
        start={this.props.start}
        onDrag={this.handleDrag}
        onStop={this.handleStop}
      >
        {h}
      </Draggable>
    );
  }
}

class ResizableImage extends React.Component {
  state = this.props.imageData;

  handleMove = position => {
    //resize but keep the proportions.
    const newWidth = position.left + HANDLE_SIZE;
    const newHeight = this.state.height * (newWidth / this.state.width);
    this.setState({ width: newWidth, height: newHeight });
    this.props.onImageChanged(this.props.id, this.state);
  };

  handleStop = (event, ui) => {
    this.setState({ x: ui.position.left, y: ui.position.top });
    this.props.onImageChanged(this.props.id, this.state);
  };

  handleDelete = () => {
    this.props.handleDelete(this.props.id);
  };

  render() {
    const rectStyle = {
      width: this.state.width,
      height: this.state.height,
      borderStyle: 'dashed',
      borderWidth: 1,
      position: 'absolute',
      borderColor: 'green',
    };

    const startPosition = {
      x: this.state.width - HANDLE_SIZE,
      y: this.state.height - HANDLE_SIZE,
    };

    const deleteStyle = {
      color: 'red',
      backgroundColor: 'white',
      lineHeight: 0.6,
      // fontSize: HANDLE_SIZE,
      position: 'absolute',
      top: 0,
      right: HANDLE_SIZE * 2,
      margin: 0,
      padding: 0,
    };

    return (
      <Draggable
        cancel=".handle"
        onStop={this.handleStop}
        start={{ x: this.state.x, y: this.state.y }}
      >
        <div style={rectStyle}>
          <Handle
            start={startPosition}
            onMove={this.handleMove}
            color="skyblue"
          />
          <div style={deleteStyle} onClick={this.handleDelete}>
            x
          </div>
        </div>
      </Draggable>
    );
  }
}

const CANVAS_EXPORT_WIDTH = 1700;
const CANVAS_EXPORT_HEIGHT = 600;

class SimpleImageEditor extends React.Component {
  static propTypes = {
    imageIds: PropTypes.array,
  };

  static defaultProps = { imageIds: [] };

  constructor(props) {
    super(props);
    if (!props.imageIds.length) {
      this.state = { images: {} };
      return;
    }

    let images = {};
    for (const imageID of props.imageIds) {
      images[imageID] = {
        width: 100,
        height: 100,
        x: 0,
        y: 0,
      };
    }
    this.state = images;
  }

  dataURLToBlob = (dataURL, mime) => {
    const byteString = atob(dataURL.split(',')[1]);
    let i = byteString.length;
    let u8arr = new Uint8Array(i);
    while (i--) {
      u8arr[i] = byteString.charCodeAt(i);
    }
    return new Blob([u8arr], { type: mime });
  };

  exportJPG = () => {
    let canvas = this.canvas;
    const { width, height } = canvas;
    canvas.width = CANVAS_EXPORT_WIDTH;
    canvas.height = CANVAS_EXPORT_HEIGHT;

    this.paint(canvas, true);
    const dataURL = canvas.toDataURL('image/jpeg', 0.8);

    //restore old size
    canvas.width = width;
    canvas.height = height;
    this.paint(canvas);

    return this.dataURLToBlob(dataURL);
  };

  componentWillReceiveProps(nextProps) {
    if (!nextProps.imageIds.length) {
      return;
    }

    const images = this.state.images;
    for (const imageID of this.props.imageIds) {
      if (images[imageID]) {
        continue;
      }

      const img = document.getElementById(imageID);
      images[imageID] = {
        width: img.width,
        height: img.height,
        x: 0,
        y: 0,
      };
    }
    this.setState(images);
  }

  handleImageChanged = (imageID, data) => {
    let imagesData = this.state.images;
    imagesData[imageID] = data;
    this.setState({ images: imagesData });
  };

  handleDelete = imageID => {
    let imageData = this.state.images;
    delete imageData[imageID];
    this.setState(imageData);
    this.props.handleDelete(imageID);
  };

  componentDidMount() {
    this.paint(this.canvas);
  }

  componentDidUpdate() {
    this.paint(this.canvas);
  }

  paint = (canvas, exporting) => {
    let ctx = canvas.getContext('2d');
    ctx.save();
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    if (exporting) {
      ctx.fillStyle = 'white';
      ctx.fillRect(0, 0, canvas.width, canvas.height);
    }

    for (let imageID in this.state.images) {
      const img = document.getElementById(imageID);
      let { x, y, width, height } = this.state.images[imageID];
      if (exporting) {
        const factor = 3.4;
        x = x * factor;
        y = y * factor;
        width = width * factor;
        height = height * factor;
      }
      ctx.drawImage(img, x, y, width, height);
    }

    ctx.restore();
  };

  render() {
    let resizableImages = [];
    for (let imageID in this.state.images) {
      resizableImages.push(
        <ResizableImage
          style={{ position: 'absolute' }}
          key={imageID}
          id={imageID}
          imageData={this.state.images[imageID]}
          handleDelete={this.handleDelete}
          onImageChanged={this.handleImageChanged}
        />
      );
    }

    const canvasStyle = {
      borderStyle: 'solid',
      borderWidth: 1,
      borderColor: 'skyblue',
      position: 'absolute',
    };

    return (
      <div id="canvas-container" style={{ position: 'absolute' }}>
        <canvas
          ref={ref => (this.canvas = ref)}
          id="canvas"
          width="500"
          height="176"
          style={canvasStyle}
        />
        {resizableImages}
      </div>
    );
  }
}

export default SimpleImageEditor;
